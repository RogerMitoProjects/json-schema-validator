'use strict';

const { SCHEMA_ERRORS } = require('./constants');
let errors = [];
let unstoppable = false;

/**
 * @description It checks whether the provided structure is compatible with the schema.
 * @param {Object} obj The object to be checked. 
 * @param {Object} schema The schema to be used.
 * @param {string | null} key The current key name.
 */
async function check(obj, schema, key = null) {
  // Check if the current value is a primitive one. If applicable, performs type/rules validations.
  if (typeof obj !== 'object') {
    if (typeof obj !== schema.type) {
      errors.push({ el: key, message: SCHEMA_ERRORS.TYPE });
    }
    // Suitable for text fields.
    if (schema.maxLength && obj.toString().length > schema.maxLength) {
      errors.push({ el: key, message: SCHEMA_ERRORS.MAX_LENGTH });
    }
    if (schema.minLength && obj.toString().length < schema.minLength) {
      errors.push({ el: key, message: SCHEMA_ERRORS.MIN_LENGTH });
    }
    // Pattern check.
    if (schema.pattern && schema.required && !new RegExp(schema.pattern).test(obj)) {
      errors.push({ el: key, message: SCHEMA_ERRORS.PATTERN });
    }
    // TODO validation for numbers (min and max values).

    // Stops the current recursive call.
    return;
  }

  let schemaKeys = Object.keys(schema);
  let i = 0, edgeLength = schemaKeys.length;

  for (; i < edgeLength; i++) {
    if (typeof obj[schemaKeys[i]] === undefined && schema[schemaKeys[i]].required === true) {
      errors.push({ el: key, message: SCHEMA_ERRORS.REQUIRED });
    }
    // Element not found inside object.
    else if (typeof obj[schemaKeys[i]] === undefined) {
      errors.push({ el: key, message: SCHEMA_ERRORS.NOT_FOUND });
    }

    if (errors.length > 0 && unstoppable) {
      return;
    }
    // Recursive call to the next nodes.
    check(obj[schemaKeys[i]], schema[schemaKeys[i]], schemaKeys[i]);
  }
}

/**
 * 
 * @param {Object} obj The target object.
 * @param {Object} schema The target schema.
 * @param {Boolean} continueOnError 
 * @callback callback Error callback. The callback's param is empty for successfull validations.
 */
module.exports = (obj, schema, callback = (_err = []) => { }, continueOnError = false) => {
  errors = [];
  unstoppable = continueOnError;
  check(obj, schema)
    .then(() => {
      callback(errors);
    })
    .catch(err => {
      console.error('Something went wrong during the execution', err);
      callback([{ message: err, el: null }]);
    });
};
